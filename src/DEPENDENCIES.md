# Dependencies for running EnnaClock code

## Before we can even build Perl 6

 * sudo apt-get install build-essential git

## Perl 6 and base modules

 * get [rakudobrew](http://rakudo.org/how-to-get-rakudo/)
 * rakudobrew build moar
 * rakudobrew build panda
 * panda update
 * panda install Task::Star

## Perl 5's Device::SerialPort module

 * sudo apt-get install libdevice-serialport-perl
 * or use [cpanm](http://cpanmin.us)

## ... used via Perl 6's Inline::Perl5

 * can't just panda install, will error out with a subtle line hinting 'cannot find -lperl' buried in there.
 * sudo apt-get install libperl-dev
 * panda install Inline::Perl5

