#include <LazySerial.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// The Teensy can go at 115200, which is apparently just what its USB Serial goes at.
// I suspect the Nano clones were only really tested at 9600 though...
#define BAUD_RATE 9600
#define PIN 6
#define NUM_LEDS 90
#define NUM_GROUPS 40
#define XMAS
#define INIT_STARFIELD_DENSITY 3

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

// Only refresh strip when we need to?
bool refresh_strip = true;

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

// minicom --device /dev/ttyUSB0
// Use "Options->Screen->Add Carriage Return: Yes" as Minicom seems to render LF as plain LF.

// Sometimes if it's a dodgy connection going in, you end up on /dev/ttyUSB1.

// check we're actually doing something, repeatedly blink led 13
uint8_t led_state = LOW;
int led_countdown_ms = 1000;
unsigned long last_time_ms = 0;

// Our serial command processor.
LazySerial::LazySerial lazy(Serial);

// We use one command to set which group(s) we are talking about.
uint8_t context_group = 0;
uint8_t context_group_len = 1;

// For PixelGroup. c++ enum < 11 doesn't specify int size.
#define FX_SINGLESHOT 0
#define FX_BOUNCE     1
#define FX_STARFIELD  2
#define FX_LIGHTNING  3
#define FX_DARKNING   4  // like LIGHTNING, but doesn't display anything.

/**
 * Handles the special effects of fading between colours for a given range of pixels on the NeoPixel strip.
 * Solid colour is handled as a special case of fading.
 */
class PixelGroup
{
public:
  explicit
  PixelGroup() {
    set_range(0, 0);
    set_rgb(0, 0, 0);
  }

  /**
   * Get pixel current RGB in packed NeoPixel format.
   */
  uint32_t
  col() {
    if (fx == FX_DARKNING) {
      return Adafruit_NeoPixel::Color(0, 0, 0);
    }
    
    int16_t currentRGB[3];   // 0-25500
    // Fade towards target.
    for (int i = 0; i < 3; ++i) {
      currentRGB[i] = lerp(i);
    }
    
    return Adafruit_NeoPixel::Color(
        currentRGB[0] / 100,
        currentRGB[1] / 100,
        currentRGB[2] / 100 );
  }

  /**
   * Set start pixel address and length of this group.
   */
  void
  set_range(uint8_t start, uint8_t len) {
    led_start = start;
    led_length = len;
  }

  /**
   * Set pixel to given RGB (0-255) and kill velocity.
   */
  void
  set_rgb(uint8_t r, uint8_t g, uint8_t b) {
    originRGB[0] = targetRGB[0] = constrain(r, 0, 255) * 100;
    originRGB[1] = targetRGB[1] = constrain(g, 0, 255) * 100;
    originRGB[2] = targetRGB[2] = constrain(b, 0, 255) * 100;
    total_steps = 1;
    pos = 1;
    dir = 0;
    fx = FX_SINGLESHOT;
    star = 0;
    update_strip();
  }

  /**
   * Set target to given RGB (0-255), and given number of steps to get us there.
   */
  void
  fade_to(uint8_t r, uint8_t g, uint8_t b, uint8_t steps) {
    // Take our 'current' colour as our new origin point.
    for (int i = 0; i < 3; ++i) {
      originRGB[i] = lerp(i);
    }
    // Our new target.
    targetRGB[0] = constrain(r, 0, 255) * 100;
    targetRGB[1] = constrain(g, 0, 255) * 100;
    targetRGB[2] = constrain(b, 0, 255) * 100;
    total_steps = steps;
    pos = 0;
    dir = 1;
    fx = FX_SINGLESHOT;
    star = 0;
    update_strip();
  }

  /**
   * Like fade_to, but once we reach the end we should head back.
   * Assumes the current colour is what you want as the origin! May want to use set_rgb first.
   */
  void
  bounce_with(uint8_t r, uint8_t g, uint8_t b, uint8_t steps) {
    // Take our 'current' colour as our new origin point.
    for (int i = 0; i < 3; ++i) {
      originRGB[i] = lerp(i);
    }
    // Our new target.
    targetRGB[0] = constrain(r, 0, 255) * 100;
    targetRGB[1] = constrain(g, 0, 255) * 100;
    targetRGB[2] = constrain(b, 0, 255) * 100;
    total_steps = steps;
    pos = 0;
    dir = 1;
    fx = FX_BOUNCE;
    star = 0;
    update_strip();
  }

  /**
   * Like bounce_with, but affects only a random pixel each time, and always fades between black.
   */
  void
  star_field(uint8_t r, uint8_t g, uint8_t b, uint8_t steps) {
    // Our origin point is assumed to always be black.
    for (int i = 0; i < 3; ++i) {
      originRGB[i] = 0;
    }
    // Our new target.
    targetRGB[0] = constrain(r, 0, 255) * 100;
    targetRGB[1] = constrain(g, 0, 255) * 100;
    targetRGB[2] = constrain(b, 0, 255) * 100;
    total_steps = steps;
    // To increase randomosity, when the animation is initialised, we start at a random point
    // along 'pos'.
    pos = random(total_steps);
    dir = 1;
    fx = FX_STARFIELD;
    star = random(led_length);
    update_strip();
  }


  /**
   * Flashes a colour, fades to black, waits before doing it again.
   */
  void
  lightning(uint8_t r, uint8_t g, uint8_t b, uint8_t steps, uint8_t chance) {
    // Our origin point is always the flash colour.
    originRGB[0] = constrain(r, 0, 255) * 100;
    originRGB[1] = constrain(g, 0, 255) * 100;
    originRGB[2] = constrain(b, 0, 255) * 100;
    // Our target point is assumed to always be black.
    for (int i = 0; i < 3; ++i) {
      targetRGB[i] = 0;
    }

    total_steps = steps;
    pos = 0;
    dir = 1;
    fx = FX_LIGHTNING;
    star = chance;
    update_strip();
  }


  /**
   * For a given 'tick', if we are fading, do one increment of fade.
   */
  void
  loop() {
    if (led_length == 0) {
      // Inactive group.
      return;
    }

    // Handle fade stop and bouncing animations.
    if (pos >= total_steps) {
      // Reached target.
      if (fx == FX_LIGHTNING || fx == FX_DARKNING) {
        // Lightning just struck. Do we do another flash, or stay quiet for a cycle?
        pos = 0;
        int lightning_strike = random(star);
        fx = lightning_strike == 0 ? FX_LIGHTNING : FX_DARKNING;
        
      } else if (fx != FX_SINGLESHOT && dir == 1) {
        // Head back now please.
        dir = -1;
        
      } else {
        // Fading; can stop here.
        return;
      }
      
    } else if (pos == 0) {
      // Just starting, or bounced back to the beginning?
      if (fx != FX_SINGLESHOT && dir == -1) {
        // Start over again.
        dir = 1;
        
        // Starfields start with a new random star each time we cycle.
        if (fx == FX_STARFIELD) {
          star = random(led_length);
        }
      }
      
    }

    // Process animation.
    pos += dir;
    update_strip();
  }


  /**
   * We are now responsible for updating the strip ourselves where appropriate.
   * strip.show() still only gets called once per main loop.
   */
  void
  update_strip() {
    if (fx == FX_STARFIELD) {
      // Only update one pixel in starfield mode. Others go dark.
      for (int i = 0; i < led_length; ++i) {
        strip.setPixelColor(led_start + i, 0);
      }
      strip.setPixelColor(led_start + star, col());
      
    } else {
      // FX_SINGLESHOT, FX_BOUNCE: Update entire group with same colour.
      for (int i = 0; i < led_length; ++i) {
        strip.setPixelColor(led_start + i, col());
      }
      
    }
    refresh_strip = true;
  }


  /**
   * Return an individual channel (0-2)'s linearly interpolated value based on pos
   */
  int16_t
  lerp(uint8_t channel) {
    int32_t deltaC = targetRGB[channel] - originRGB[channel];
    // ordinarily we would multiply deltaC by the ratio of pos/total_steps, but INTEGER MATHS!
    deltaC *= pos;
    deltaC /= total_steps;
    return originRGB[channel] + deltaC;
  }


  uint8_t led_start;
  uint8_t led_length;
  int16_t originRGB[3];    // 0-25500
  int16_t targetRGB[3];    // 0-25500
  uint8_t total_steps;     // 1-50ish? Depends on our tick length.
  uint8_t pos;             // 0-50ish? Depends on our tick length.
  int8_t  dir;             // -1..1
  uint8_t fx;              // from FX_ defines above.
  uint8_t star;            // 0..^led_length for FX_STARFIELD. Represents chance of strike for FX_LIGHTNING.
};

PixelGroup groups[NUM_GROUPS];


// SERIAL COMMAND CALLBACKS

void say_hello(char *blah)
{
  Serial.println(F("OHAI"));
}


// Command to set which group(s) we are talking about.
void cmd_group(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into group number and length to set our 'context' for further operations.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is GROUP num (optional length) where num is 0..^NUM_GROUPS and length is 1..NUM_GROUPS.
  // Range is reduced automatically to make it stay on the group array.
  if ( ! pos) {
    Serial.println(F("Usage: GROUP num (, howmany)"));
    return;
  }
  
  if (strcasecmp(pos, "ALL") == 0) {
    // Use magic phrase "GROUP ALL" to select everything. Handy to clear FX.
    context_group = 0;
    context_group_len = NUM_GROUPS;
    
  } else {
    context_group = constrain(atoi(pos), 0, NUM_GROUPS - 1);
    context_group_len = 1;
  
    // get optional length
    pos = strtok(NULL, delim);
    if (pos) {
      context_group_len = constrain(atoi(pos), 1, NUM_GROUPS - context_group);
    }
  }

  // Group context now set.
  Serial.print(F("OK Groups "));
  Serial.print(context_group);
  Serial.print(F(".."));
  Serial.println(context_group + context_group_len - 1);
}


// Command to set which LED(s) the current group controls.
void cmd_leds(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into group number and length to set our 'context' for further operations.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is LEDS num (optional length) where num is 0..^strip.numPixels() and length is 0..strip.numPixels().
  // Range is reduced automatically to make it stay on the group array.
  // Zero length ranges are permissible.
  if ( ! pos) {
    Serial.println(F("Usage: LEDS lednum (, howmany)"));
    return;
  }
  
  uint8_t led_start, led_length;
  if (strcasecmp(pos, "ALL") == 0) {
    // Use magic phrase "LEDS ALL" to select everything. Handy to test with.
    led_start = 0;
    led_length = strip.numPixels();
    
  } else {
    led_start = constrain(atoi(pos), 0, strip.numPixels()-1);
    led_length = 1;
    
    // get optional length
    pos = strtok(NULL, delim);
    if (pos) {
      led_length = constrain(atoi(pos), 0, strip.numPixels() - led_start);
    }
  }
  
  // Set it on our current groups
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].set_range(led_start, led_length);
  }
  
  Serial.print(F("OK LEDs "));
  Serial.print(led_start);
  Serial.print(F(".."));
  Serial.println(led_start + led_length - 1);
}


// Command to set a group to a specific RGB colour, no fade.
void cmd_set(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into RGB values to control the LED strip.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is SET r g b  where the group is pre-set by the GROUP command and rgb values are 0..255.
  if ( ! pos) {
    Serial.println(F("Usage: SET red, green, blue"));
    return;
  }
  // Red
  uint8_t red = constrain(atoi(pos), 0, 255);

  // Green
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t green = constrain(atoi(pos), 0, 255);

  // Blue
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t blue = constrain(atoi(pos), 0, 255);

  // Set colours on our PixelGroup array.
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].set_rgb(red, green, blue);
  }

  Serial.print(F("OK SET "));
  Serial.print(red);
  Serial.print(" ");
  Serial.print(green);
  Serial.print(" ");
  Serial.println(blue);
}


// Command to set a specific RGB colour to fade to, over n ticks.
void cmd_fade(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into RGB values to control the LED strip.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is FADE r g b n  where group is pre-set by the GROUP command and rgb n values are 0..255.
  if ( ! pos) {
    Serial.println(F("Usage: FADE red, green, blue, numticks"));
    return;
  }
  // Red
  uint8_t red = constrain(atoi(pos), 0, 255);

  // Green
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t green = constrain(atoi(pos), 0, 255);

  // Blue
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t blue = constrain(atoi(pos), 0, 255);

  // Number of ticks
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t numticks = constrain(atoi(pos), 0, 255);

  // Set animation on our PixelGroup array.
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].fade_to(red, green, blue, numticks);
  }

  Serial.print(F("OK FADE "));
  Serial.print(red);
  Serial.print(" ");
  Serial.print(green);
  Serial.print(" ");
  Serial.print(blue);
  Serial.print(" ");
  Serial.println(numticks);
}


// Command to set a specific RGB colour, then fade to black over n ticks.
void cmd_flash(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into RGB values to control the LED strip.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is FLASH r g b n  where group is pre-set by the GROUP command and rgb n values are 0..255.
  if ( ! pos) {
    Serial.println(F("Usage: FLASH red, green, blue, numticks"));
    return;
  }
  // Red
  uint8_t red = constrain(atoi(pos), 0, 255);

  // Green
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t green = constrain(atoi(pos), 0, 255);

  // Blue
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t blue = constrain(atoi(pos), 0, 255);

  // Number of ticks
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t numticks = constrain(atoi(pos), 0, 255);

  // Set animation on our PixelGroup array.
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].set_rgb(red, green, blue);
    groups[context_group + i].fade_to(0, 0, 0, numticks);
  }

  Serial.print(F("OK FLASH "));
  Serial.print(red);
  Serial.print(" ");
  Serial.print(green);
  Serial.print(" ");
  Serial.print(blue);
  Serial.print(" ");
  Serial.println(numticks);
}


// Command to set a specific RGB colour to bounce with, over n ticks.
void cmd_bounce(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into RGB values to control the LED strip.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is BOUNCE r g b n  where group is pre-set by the GROUP command and rgb n values are 0..255.
  if ( ! pos) {
    Serial.println(F("Usage: BOUNCE red, green, blue, numticks"));
    return;
  }
  // Red
  uint8_t red = constrain(atoi(pos), 0, 255);

  // Green
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t green = constrain(atoi(pos), 0, 255);

  // Blue
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t blue = constrain(atoi(pos), 0, 255);

  // Number of ticks
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t numticks = constrain(atoi(pos), 0, 255);

  // Set animation on our PixelGroup array.
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].bounce_with(red, green, blue, numticks);
  }

  Serial.print(F("OK BOUNCE "));
  Serial.print(red);
  Serial.print(" ");
  Serial.print(green);
  Serial.print(" ");
  Serial.print(blue);
  Serial.print(" ");
  Serial.println(numticks);
}


// Command to do a starfield animation.
void cmd_starfield(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into RGB values to control the LED strip.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is STARFIELD r g b n  where group is pre-set by the GROUP command and rgb n values are 0..255.
  if ( ! pos) {
    Serial.println(F("Usage: STARFIELD red, green, blue, numticks"));
    return;
  }
  // Red
  uint8_t red = constrain(atoi(pos), 0, 255);

  // Green
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t green = constrain(atoi(pos), 0, 255);

  // Blue
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t blue = constrain(atoi(pos), 0, 255);

  // Number of ticks
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t numticks = constrain(atoi(pos), 0, 255);

  // Set animation on our PixelGroup array.
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].star_field(red, green, blue, numticks);
  }

  Serial.print(F("OK STARFIELD "));
  Serial.print(red);
  Serial.print(" ");
  Serial.print(green);
  Serial.print(" ");
  Serial.print(blue);
  Serial.print(" ");
  Serial.println(numticks);
}


// Command to do a lightning animation.
void cmd_lightning(char *args)
{
  // Here we use strtok() and atoi() to rip out all comma-separated values from the args string,
  // and turn them into RGB values to control the LED strip.
  char delim[] = " ,";
  char *pos;

  // Initialise strtok with the string once. Subsequent calls pass NULL to re-use strtok's stored version.
  pos = strtok(args, delim);

  // Syntax is LIGHTNING r g b n c where group is pre-set by the GROUP command and rgb n values are 0..255.
  if ( ! pos) {
    Serial.println(F("Usage: LIGHTNING red, green, blue, numticks, chance"));
    return;
  }
  // Red
  uint8_t red = constrain(atoi(pos), 0, 255);

  // Green
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t green = constrain(atoi(pos), 0, 255);

  // Blue
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t blue = constrain(atoi(pos), 0, 255);

  // Number of ticks to do the fade to black
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t numticks = constrain(atoi(pos), 0, 255);

  // Chance of strike; e.g. if == 6, 1d6 chance that this lightning cycle will be a visible flash.
  // Increase to spend more cycles dark.
  pos = strtok(NULL, delim);
  if ( ! pos) { return; }
  uint8_t chance = constrain(atoi(pos), 0, 255);

  // Set animation on our PixelGroup array.
  for (int i = 0; i < context_group_len; ++i) {
    groups[context_group + i].lightning(red, green, blue, numticks, chance);
  }

  Serial.print(F("OK LIGHTNING "));
  Serial.print(red);
  Serial.print(" ");
  Serial.print(green);
  Serial.print(" ");
  Serial.print(blue);
  Serial.print(" ");
  Serial.println(numticks);
  Serial.print(" ");
  Serial.println(chance);
}


// ==============================================  MAIN ARDUINO FUNCTIONS  ==================================================

void setup()
{
  last_time_ms = millis();

  // register some commands for the serial interface.
  lazy.register_callback("HELLO",     &say_hello);
  lazy.register_callback("GROUP",     &cmd_group);
  lazy.register_callback("LEDS",      &cmd_leds);
  lazy.register_callback("SET",       &cmd_set);
  lazy.register_callback("FADE",      &cmd_fade);
  lazy.register_callback("FLASH",     &cmd_flash);
  lazy.register_callback("BOUNCE",    &cmd_bounce);
  lazy.register_callback("STARFIELD", &cmd_starfield);
  lazy.register_callback("LIGHTNING", &cmd_lightning);

  // We still need to do our own Serial init.
  Serial.begin(BAUD_RATE);

  // And initialise the NeoPixel strip.
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  // Check that Adafruit_NeoPixel was able to alloc all those pixels.
  if (strip.numPixels() > 0) {
    // Construct an idle starfield animation to default to,
    // to show we are powered but have received no commands.
    for (int i = 0; i < strip.numPixels() / INIT_STARFIELD_DENSITY; ++i) {
      uint8_t led_start = i * INIT_STARFIELD_DENSITY;
      uint8_t led_length = constrain(INIT_STARFIELD_DENSITY, 0, strip.numPixels() - led_start);
      groups[i].set_range(led_start, led_length);
#ifdef XMAS
      // XMAS MODE
      if (i % 2 == 0) {
        groups[i].star_field(128, 0, 0, 30);
      } else {
        groups[i].star_field(0, 128, 0, 30);
      }
#else
      groups[i].star_field(128, 128, 128, 30);
#endif
    }

  } else {
    // We ran out of memory. NeoPixel does dynamical allocation so it's the first to suffer.
    // Re-init to 1 pixel.
    strip.updateLength(1);
    strip.begin();
    // Flash a warning light.
    groups[0].set_range(0, 1);
    groups[0].set_rgb(255, 0, 0);
    groups[0].bounce_with(0, 0, 0, 120);
  }
}


void loop()
{
  unsigned long current_time_ms = millis();
  
  // so we know it's doing something
  led_countdown_ms -= current_time_ms - last_time_ms;
  if (led_countdown_ms < 0) {
    if (led_state == HIGH) {
      led_state = LOW;
      led_countdown_ms = 500;
    } else {
      led_state = HIGH;
      led_countdown_ms = 1000;
    }
    digitalWrite(13, led_state);   // set the internal LED
  }
  last_time_ms = current_time_ms;

  // Process commands via LazySerial!
  lazy.loop();

  // Process pixel group FX
  for (int i = 0; i < NUM_GROUPS; ++i) {
    groups[i].loop();
  }
  // Refresh strip only if 'dirty'
  if (refresh_strip) {
    strip.show();
    refresh_strip = false;
  }

  // Tick delay - allow Adafruit_Neopixel lib to do whatever PWMing it's doing in the bg (maybe?),
  // and act as our 'framerate' for fade effects.
  // Delay FPS
  // 20    50
  // 33   ~30
  // 50    20
  // 100   10
  delay(33);
}

