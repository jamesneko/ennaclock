#!/usr/bin/perl -CSDA

use warnings;
use strict;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

use FindBin qw($RealBin);
use lib "$RealBin/lib5";

use Getopt::Long;
use Term::ANSIColor;
use Try::Tiny;

use Helpers;
use NanoPixels;
use DummyNanoPixels;
use WordTime;
use WeatherForecast;
use WeatherDisplay;
use SystemStatus;
use ClockDisplay;

my %opts = ();
GetOptions(\%opts, "help!", "one-shot!", "dummy-plug!", "just-display=s", "random-colours!");

my $CFG_DIR = "$RealBin/cfg";


sub help()
{
	print STDERR <<EOF;
Usage: $0
	--one-shot : Test one iteration only and exit.
	--dummy-plug : Simulate calls to a fake Arduino Nano.
	--just-display : Light up particular words.
	--random-colours : Words will pick random colours.
EOF
	exit 1;
}


sub main()
{
	say colored("---------------- ENNA CLOCK ----------------", 'bold yellow');
	
	my $nano;
	if ($opts{'dummy-plug'}) {
		say colored("Instantiating ", 'bold blue'), colored("Dummy Plug System", 'bold red');
		$nano = DummyNanoPixels->new();
	} else {
		say colored("Instantiating NanoPixels", 'bold blue');
		$nano = NanoPixels->new();
	}

	say colored("Instantiating WordTime", 'bold blue');
	my $wordtime = WordTime->new();
	use_random_colours($wordtime) if $opts{'random-colours'};

	say colored("Instantiating WeatherForecast", 'bold blue');
	my $apikey = slurp($CFG_DIR . '/openweathermap.apikey') or die "Couldn't read cfg/openweathermap.apikey!";
	chomp $apikey;
	my $weatherf = WeatherForecast->new(apikey => $apikey);

	say colored("Instantiating WeatherDisplay", 'bold blue');
	my $weatherd = WeatherDisplay->new(weather => $weatherf);

	say colored("Instantiating SystemStatus", 'bold blue');
	my $sysstat = SystemStatus->new();
	
	say colored("Instantiating ClockDisplay", 'bold blue');
	my $clock = ClockDisplay->new(
			led_cfg_file => "$CFG_DIR/leds.json",
			nano => $nano,
			wordtime => $wordtime,
			weather_forecast => $weatherf,
			weather_display => $weatherd,
			sysstat => $sysstat,
		);
	
	while (1) {
		say colored("Refreshing...", 'cyan');
		$clock->refresh();
		$clock->transition(parse_state($opts{'just-display'})) if $opts{'just-display'};
		last if $opts{'one-shot'};
		sleep 10;
	}
}


sub parse_state($statestr)
{
	my $colour = "#FFFFFF";
	my $state = {};
	foreach my $word (split ' ', $statestr) {
		if ($word =~ /#/) {
			$colour = $word;
		} else {
			$state->{$word} = $colour;
		}
	}
	return $state;
}


sub use_random_colours($wordtime)
{
	foreach my $word (keys %{$wordtime->word_colours}) {
		$wordtime->word_colours->{$word} = "RANDOM";
	}
}


help() if $opts{help};
main();
