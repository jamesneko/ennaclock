package Helpers;
# Helper subs to do helpful things.

use warnings;
use strict;
use utf8;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

require Exporter;
our @ISA = qw/Exporter/;
# use @EXPORT_OK to list the symbols to export on request only. @EXPORT is everything that gets exported by default.
our @EXPORT = qw/note slurp spew hex2rgb rgb2hex lerp random_hex_colour/;
our %EXPORT_TAGS = ( all => [@EXPORT] );


sub note
{
	say STDERR @_;
}


sub slurp($filename)
{
	open(my $fh, '<:utf8', $filename) or die "Couldn't read from '$filename' : $!\n";
	local $/ = undef;       # unset record separator to slurp entire file in one readline call.
	my $contents = readline $fh;
	close($fh) or die "Couldn't close file '$filename' : $!\n";

	return $contents;
}


sub spew($filename, $contents)
{
	open(my $fh, '>:utf8', $filename) or die "Couldn't write to '$filename' : $!\n";
	print $fh $contents or die "Couldn't write to '$filename' : $!\n";
	close($fh) or die "Couldn't close file '$filename' : $!\n";
}


# Go from #FFFFFF -> 255, 255, 255
sub hex2rgb($colour)
{
	if ($colour =~ /([0-9A-F]{2})([0-9A-F]{2})([0-9A-F]{2})/i) {
		return hex($1), hex($2), hex($3);
	} else {
		return 0, 0, 0;
	}
}


# Go from #FFFFFF -> 255, 255, 255
sub rgb2hex($r, $g, $b)
{
	return sprintf("#%2.2X%2.2X%2.2X", $r, $g, $b);
}


# Linearly Interpolate between two hex colours for some number on some range.
sub lerp($value, $min, $max, $min_colour, $max_colour)
{
	# Clamp onto given range.
	return $min_colour if $value <= $min;
	return $max_colour if $value >= $max;
	
	# Fraction of our progress from $min to $max
	$max = $min + 1 if $max == $min;
	my $frac = ($value - $min) / ($max - $min);
	my @minrgb = hex2rgb $min_colour;
	my @maxrgb = hex2rgb $max_colour;
	my @lerped = map { ($maxrgb[$_] - $minrgb[$_]) * $frac + $minrgb[$_] } (0,1,2);
	return rgb2hex @lerped;
}


# Return a random colour (but not one that's too dark)
sub random_hex_colour()
{
	my ($r, $g, $b) = (0, 0, 0);
	while ($r + $g + $b < 96 * 3) {
		$r = int rand 256;
		$g = int rand 256;
		$b = int rand 256;
	}
	return rgb2hex($r, $g, $b);
}


1;