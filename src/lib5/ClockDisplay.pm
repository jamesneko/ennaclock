package ClockDisplay;

# Using our configured LED mapping, take data sources of WordTime and WeatherForecast and use NanoPixels to issue
# appropriate commands to get our display to the state we want.

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

use Term::ANSIColor;
use Try::Tiny;
use JSON;
use Data::Dumper;

use Helpers;
use Cooldown qw/cooldown/;
use NanoPixels;
use DummyNanoPixels;
use WordTime;
use WeatherForecast;
use WeatherDisplay;
use SystemStatus;


has nano             => ( is => 'rw', isa => 'NanoPixels | DummyNanoPixels', required => 1 );
has wordtime         => ( is => 'rw', isa => 'WordTime', required => 1 );
has weather_forecast => ( is => 'rw', isa => 'WeatherForecast', required => 1 );
has weather_display  => ( is => 'rw', isa => 'WeatherDisplay', required => 1 );
has sysstat          => ( is => 'rw', isa => 'SystemStatus', required => 1 );

has led_cfg_file     => ( is => 'rw', isa => 'Str', required => 1 );
has led_cfg_mtime    => ( is => 'rw', isa => 'Int', default => 0 );

has nano_first_init  => ( is => 'rw', isa => 'Bool', default => 1 );	# Is this the first time we've initialised it at all? (i.e. be fancy?)
has nano_needs_init  => ( is => 'rw', isa => 'Bool', default => 1 );	# Do we need to (re)init the groups? (i.e. unplug/overload event)

# led group name => {
#	group=>,
#	start=>,
#	length=>,
#  colour=>,	# CURRENT colour state that the clock is on far as we know, #FFFFFF, might use special names like 'rain', 'lightning2'
#}
has leds            => ( is => 'rw', isa => 'HashRef', default => sub { { } } );


# Check if our config files need reloading, refresh other modules if need be, do things and stuff.
sub refresh($self)
{
	# (re)load leds.json
	try {
		my $led_cfg_current_mtime = (stat $self->led_cfg_file)[9];
		if ($led_cfg_current_mtime != $self->led_cfg_mtime) {
			note "Loading " . $self->led_cfg_file . "...";
			$self->load_cfg;
		}
	} catch {
		note colored("Failed to load LED configuration:-\n", 'bold red') . colored($_, 'red');
		$self->blank();
		#### Can we blink LED 0 or something?
	};
	
	# (re)initialise NanoPixel display, for example after connection dropped.
	if ($self->nano_needs_init) {
		try {
			note colored("Initialising NanoPixels...", 'bold cyan');
			$self->nano->init();
			
			note colored(" - Connecting...", 'bold cyan');
			$self->nano->connect();

			note colored(" - Configuring...", 'bold cyan');
			$self->configure_groups($self->nano_first_init);
			
			# Do an initial render call for full state
			note colored(" - Setting current state...", 'bold cyan');
			my $state = $self->get_current_state();
			$self->render($state);
			
			# Success!
			$self->nano_first_init(0);
			$self->nano_needs_init(0);
		} catch {
			note colored("Failed to initialise NanoPixels:-\n", 'bold red') . colored($_, 'red');
		};
	}
	
	# Make a copy of what the current state is supposed to be, modify it, transition to it.
	my $state = $self->get_current_state();
	
	# Process WordTime module:-
	try {
		$self->wordtime->render_into($state);
	} catch {
		note colored("Error while processing WordTime:-\n", 'bold red') . colored($_, 'red');
	};
	
	# Process Weather modules:-
	try {
		$self->weather_forecast->refresh() if cooldown("weather check", 3600);
		$self->weather_display->render_into($state);
	} catch {
		note colored("Error while processing Weather modules:-\n", 'bold red') . colored($_, 'red');
	};
	
	# Process SystemStatus module:-
	try {
		$self->sysstat->render_into($state);
	} catch {
		note colored("Error while processing SystemStatus:-\n", 'bold red') . colored($_, 'red');
	};

	# Transition to new state (if any changes)
	$self->transition($state);
}


# Takes hashref of led group name => "#colour or special"
# Non-entries are assumed to be black.
sub transition($self, $new_state)
{
	# Work out what we need to change.
	my $change = {};
	foreach my $gn (keys %{$self->leds}) {
		if ($self->leds->{$gn}->{colour} ne ($new_state->{$gn} // "#000000")) {
			$change->{$gn} = $new_state->{$gn} // "#000000";
			note "Transition: $gn to $change->{$gn}";
		}
	}
	
	# Do the minimum changes.
	$self->render($change);
}


# Runs commands to make this state (everything, or partial) happen.
sub render($self, $state)
{
	# NanoPixels can die() if the connection seems to have dropped.
	# We catch that here, so we can force a re-init from the full state.
	try {
		foreach my $gn (keys %$state) {
			my $colour = $state->{$gn};
			
			if ($colour =~ /^#[0-9A-Z]{6}$/i) {
				$self->fade($gn, $colour, 20);
			} elsif ($colour =~ /^BOUNCE(#[0-9A-Z]{6})(#[0-9A-Z]{6})\/(\d+)$/i) {
				$self->bounce($gn, $1, $2, $3);
			} elsif ($colour =~ /^STARFIELD(#[0-9A-Z]{6})\/(\d+)$/i) {
				$self->starfield($gn, $1, $2);
			} elsif ($colour =~ /^RANDOM$/i) {
				$self->fade($gn, random_hex_colour(), 20);
			}
			
			# Update what we know to be the state.
			$self->leds->{$gn}->{colour} = $colour;
		}
	} catch {
		note colored("render() died, possibly due to NanoPixels:-", 'bold red') . colored($_, 'red');
		$self->nano_needs_init(1);
	};
}


# Returns a hash led group name => '#FFFFFF'etc, based on what we assert the current state must be.
sub get_current_state($self)
{
	my %state = map { $_ => $self->leds->{$_}->{colour} } keys %{$self->leds};
	return \%state;
}


sub load_cfg($self)
{
	$self->led_cfg_mtime((stat $self->led_cfg_file)[9]);
	my $json = slurp($self->led_cfg_file);
	my $hash = from_json $json;
	
	$self->leds($hash);
	# Add extra fields we expect to use at runtime but not used in .json file
	$self->blank();
}


sub blank($self)
{
	my $leds = $self->leds;
	foreach my $group (values %$leds) {
		$group->{colour} = "#000000";
	}
}


# Send suitable NanoPixel commands to (re)initialise our groups.
sub configure_groups($self, $be_fancy)
{
	my $leds = $self->leds;
	foreach my $gn (keys %$leds) {
		my $groupid = $leds->{$gn}->{group};
		my $start = $leds->{$gn}->{start};
		my $length = $leds->{$gn}->{length};

		$self->nano->group($groupid);
		$self->nano->leds($start, $length);
		if ($be_fancy) {
			$self->nano->set(255, 255, 255);
			$self->nano->fade(0, 0, 0, 20);
		}
	}
}


# These internal functions mimic their NanoPixel counterparts but take named groups and handle that automatically,
# and use hex codes instead of r,g,b
sub set($self, $groupname, $colour)
{
	my $g = $self->leds->{$groupname};
	$self->nano->group($g->{group});
	$self->nano->set(hex2rgb($colour));
}


sub fade($self, $groupname, $colour, $ticks)
{
	my $g = $self->leds->{$groupname};
	$self->nano->group($g->{group});
	$self->nano->fade(hex2rgb($colour), $ticks);
}


# this version of bounce also explicitly specifies the two colours to bounce between.
sub bounce($self, $groupname, $colour1, $colour2, $ticks)
{
	my $g = $self->leds->{$groupname};
	$self->nano->group($g->{group});
	$self->nano->set(hex2rgb($colour1));
	$self->nano->bounce(hex2rgb($colour2), $ticks);
}


sub starfield($self, $groupname, $colour, $ticks)
{
	my $g = $self->leds->{$groupname};
	$self->nano->group($g->{group});
	$self->nano->starfield(hex2rgb($colour), $ticks);
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
