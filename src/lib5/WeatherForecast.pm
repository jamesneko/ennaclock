package WeatherForecast;

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

use List::Util;	# Not importing 'min' and 'max' to avoid conflict with ours.
use Try::Tiny;
use LWP::UserAgent;
use JSON;

use Helpers;


has apikey     => ( is => 'rw', isa => 'Str', required => 1 );
has location   => ( is => 'rw', isa => 'Str', default => "Sydney,au" );
has owm_result => ( is => 'rw' );
has bom_max    => ( is => 'rw' );
has bom_uv     => ( is => 'rw' );
has bom_storm  => ( is => 'rw' );
has debug      => ( is => 'rw', isa => 'Bool', default => 1 );



sub refresh($self)
{
	try {
		$self->refresh_owm();
	} catch {
		warn "Error while fetching OpenWeatherMap data: $_";
	};
	try {
		$self->refresh_bom();
	} catch {
		warn "Error while fetching Bureau of Meteorology data: $_";
	};
}


sub refresh_owm($self)
{
	my $lwp = LWP::UserAgent->new;
	my $url = "http://api.openweathermap.org/data/2.5/forecast?"
					. "q=" . $self->location
					. "&appid=" . $self->apikey
					. "&units=metric";
	note "GET: $url";
	my $response = $lwp->get($url);
	
	die $response->status_line unless $response->is_success;
	my $text = $response->decoded_content;
	#note "Response: " . $text;
	
	$self->owm_result(from_json $text);
	note "";

	if ($self->debug) {
		# List weather for each datetime entry.
		# Separate by day.
		#note "Dumper: " . Dumper($self->owm_result);
		my $day;
		foreach my $entry (@{$self->owm_result->{list}}) {
			my $etime = $entry->{dt};
			my ($etime_sec, $etime_min, $etime_hour, $etime_day) = localtime($etime);
			
			$day //= $etime_day;
			if ($day != $etime_day) {
				note "";
				$day = $etime_day;
			}
			my $min  = sprintf("%0.2f", $entry->{main}->{temp_min});
			my $max  = sprintf("%0.2f", $entry->{main}->{temp_max});
			my $wind = sprintf("%0.2f", $entry->{wind}->{speed});
			# See http://openweathermap.org/weather-conditions for all weather codes.
			my @weathers = map { "$_->{id} $_->{main} - $_->{description}" } @{$entry->{weather}};
			note scalar localtime($etime) . ": $min - $max, $wind m/s, ", @weathers;
		}
	}
}


# Check the NSW BOM.
# May break easily, should not be relied on.
# Some data is available at e.g. ftp://ftp.bom.gov.au/anon/gen/fwo/IDN11060.xml,
# See: http://www.bom.gov.au/catalogue/data-feeds.shtml
# But it's all over the place. Scraping is the only easy way to get at UV it seems.
# Try to extract the raw text between qq!<div class="day! (main|eve etc) and qq!<div class="(first|day)! ?
sub refresh_bom($self)
{
	die "Unknown location format: " . $self->location unless $self->location =~ /^([^,]+),/;
	my $city = lc $1;
	
	my $lwp = LWP::UserAgent->new;
	my $url = "http://www.bom.gov.au/nsw/forecasts/" . $city . ".shtml";
	note "GET: $url";
	my $response = $lwp->get($url);
	
	die $response->status_line unless $response->is_success;
	my $text = $response->decoded_content;
	#note "Response: " . $text;
	
	if ($text =~ /
	             <div\sclass="day[^>]+>
	             (.*?)
	             <div\sclass="(first|day)"
	             /xsm) {
	   my $para = $1;
		#note "Matching Response: $para";
		
		# Extract Max temperature predicted by BOM; usually same as OWM but when it gets higher than 25, BOM seems more accurate.
		if ($para =~ m!<em class="max">(\d+)</em>!i) {
			note "BOM Max Temperature: $1";
			$self->bom_max($1);
		} else {
			note "BOM Max Temperature: Cannot scrape from BOM site.";
			$self->bom_max(undef);
		}
		
		# Extract any UV warning index mentioned; not always present, and usually EXTREME.
		if ($para =~ m!UV Index predicted to reach (\d+)!i) {
			note "BOM UV Warning: $1";
			$self->bom_uv($1);
		} else {
			note "BOM UV Warning: None";
			$self->bom_uv(undef);
		}

		# Extract any Thunderstorm mentions in the description. Not super accurate.
		if ($para =~ m!(the chance of a thunderstorm|thunderstorm)!i) {
			note "BOM Thunderstorm Warning: $1";
			$self->bom_storm($1);
		} else {
			note "BOM Thunderstorm Warning: None";
			$self->bom_storm(undef);
		}
		
	} else {
		note "No match in BOM text response.";
	}
}


sub today($self)
{
	my @results;
	return @results unless $self->owm_result;
	my $day;
	my $hour = (localtime)[2];
	foreach my $entry (@{$self->owm_result->{list}}) {
		my $etime = $entry->{dt};
		my ($etime_sec, $etime_min, $etime_hour, $etime_day) = localtime($etime);
		
		$day //= $etime_day;
		if ($day != $etime_day) {
			# We only care about the first day's results.
			last;
		}
		if ($hour - 3 > $etime_hour) {
			# OWM gives us forecasts in 3-hour blocks; ignore the ones that have passed for today.
			next;
		}
		my $min  = sprintf("%0.2f", $entry->{main}->{temp_min});
		my $max  = sprintf("%0.2f", $entry->{main}->{temp_max});
		my $wind = sprintf("%0.2f", $entry->{wind}->{speed});
		# See http://openweathermap.org/weather-conditions for all weather codes.
		my @weathers = map { "$_->{id} $_->{main} - $_->{description}" } @{$entry->{weather}};
		#note "-- $etime: $min - $max, $wind m/s, ", @weathers;
		push @results, {
				time => $etime,
				day => $day,
				min => $min,
				max => $max,
				wind => $wind,
				weather => $weathers[0],
			};
	}
	return @results;
}


sub cloudiness($self)
{
	my @entries = $self->today();
	return "UNKNOWN" unless @entries;
	
	my $cloudcount = 0;
	foreach my $w (map { $_->{weather} } @entries) {
		if ($w =~ /overcast clouds/) {
			$cloudcount += 3;
		} elsif ($w =~ /broken clouds/) {
			$cloudcount += 2;
		} elsif ($w =~ /clouds/) {
			$cloudcount += 1;
		} elsif ($w =~ /rain/) {
			$cloudcount += 2;
		}
	}
	
	return "VERY"   if $cloudcount >= 10;
	return "MEDIUM" if $cloudcount >= 5;
	return "FEW"    if $cloudcount >= 2;
	return "CLEAR";
}


sub raininess($self)
{
	my @entries = $self->today();
	return "UNKNOWN" unless @entries;

	my $raincount = 0;
	my $freezingcount = 0;
	foreach my $w (map { $_->{weather} } @entries) {
		if ($w =~ /extreme rain/) {
			$raincount += 4;
		} elsif ($w =~ /(heavy intensity|heavy) rain/) {
			$raincount += 3;    # 'with heavy rain' also shows up in Thunderstorm codes.
		} elsif ($w =~ /moderate rain/) {
			$raincount += 2;
		} elsif ($w =~ /(light|shower) rain/) {
			$raincount += 1;
		} elsif ($w =~ /drizzle/) {
			$raincount += 1;   # found in the Thunderstorm codes.
		} elsif ($w =~ /freezing rain/) {
			$raincount += 1;
			$freezingcount += 1;
		}
	}
	
	return "TORRENTIAL" if $raincount >= 15;
	return "HEAVY"      if $raincount >= 10;
	return "FREEZING"   if $freezingcount >= 2;
	return "MODERATE"   if $raincount >= 5;
	return "LIGHT"      if $raincount >= 1;
	return "NONE";
}


sub windiness($self)
{
	my @entries = $self->today();
	return "UNKNOWN" unless @entries;

	# Extract and Reduce wind speeds using List::Util max.
	my $maxwind = List::Util::max map { $_->{wind} } @entries;
	return "HURRICANE" if $maxwind >= 7;
	return "STRONG"    if $maxwind >= 5;
	return "WINDY"     if $maxwind >= 4;
	return "BREEZE"    if $maxwind >= 1;
	return "CALM";
}


sub storminess($self)
{
	my @entries = $self->today();
	return "UNKNOWN" unless @entries;

	my $storm = 0;
	foreach my $w (map { $_->{weather} } @entries) {
		if ($w =~ /(heavy|ragged) thunderstorm/) {
			$storm += 3;
		} elsif ($w =~ /light thunderstorm/) {
			$storm += 1;
		} elsif ($w =~ /thunderstorm/) {
			$storm += 2;
		}
	}
	
	# Stupid OWM for my region seems to never report thunderstorm codes. Fallback to scraping BOM.
	if ($storm == 0 && $self->bom_storm) {
		if ($self->bom_storm =~ /chance/) {
			$storm = 1;
		} else {
			$storm = 2;
		}
	}
	
	# Untested, no storm yet.
	return "BIG_STORM" if $storm >= 3;
	return "STORM"     if $storm >= 2;
	return "LIGHT"     if $storm >= 1;
	return "NONE";
}


sub snowiness($self)
{
	my @entries = $self->today();
	return "UNKNOWN" unless @entries;

	my $snow = 0;
	foreach my $w (map { $_->{weather} } @entries) {
		if ($w =~ /snow/) {
			$snow += 1;
		}
	}
	
	return "SNOW"   if $snow >= 1;   # Unlikely to ever see this one in Sydney =(
	return "NONE";
}


sub min($self)
{
	my @entries = $self->today();
	return 0 unless @entries;

	# Extract and Reduce temperatures using List::Util min.
	my $min = List::Util::min map { $_->{min} } @entries;
	return $min;
}


sub max($self)
{
	my @entries = $self->today();
	# Extract and Reduce temperatures using List::Util max.
	my $max = List::Util::max map { $_->{max} } @entries;
	return List::Util::max $max // 0, $self->bom_max // 0;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
