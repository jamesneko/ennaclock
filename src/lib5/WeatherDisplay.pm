package WeatherDisplay;

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';


use Helpers;
use WeatherForecast;

has weather         => ( is => 'rw', isa => 'WeatherForecast', required => 1 );
has weather_colours => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		SUN       => "#FFFFFF",
                      		SUN_UV    => "#FFFFFF",
                      		CLOUDY    => "#FFFFFF",
                      		RAINCLOUD => "#FFFFFF",
                      		RAINLIGHT => "#FFFFFF",
                      		RAINHEAVY => "#FFFFFF",
                      		LIGHTNING => "#FFFFFF",
                      		WINDY     => "#FFFFFF",
                      		SNOWFLAKE => "#FFFFFF",
                      	}
                      } );
has cloud_colours   => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		VERY      => "#FFFFFF",
                      		FEW       => "#FFFFFF",
                      		MEDIUM    => "#FFFFFF",
                      		CLEAR     => "#000000",
                      		UNKNOWN   => "#000000",
                      	}
                      } );
has rain_colours    => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		TORRENTIAL => "#FFFFFF,BOUNCE#55CCFF#77EEFF/20,BOUNCE#55CCFF#77EEFF/20",
                      		HEAVY      => "#FFFFFF,BOUNCE#5555FF#7777FF/40,BOUNCE#5555FF#7777FF/40",
                      		FREEZING   => "#FFFFFF,#AAFFFF,#000000",
                      		MODERATE   => "#DDDDDD,#55CCFF,#000000",
                      		LIGHT      => "#888888,STARFIELD#5555FF/40,#000000",
                      		NONE       => "#000000,#000000,#000000",
                      		UNKNOWN    => "#000000,#000000,#000000",
                      	}
                      } );
has storm_colours   => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		BIG_STORM => "#FFF0DD",	#### Need special FX codes for this.
                      		STORM     => "#FFF0DD",
                      		LIGHT     => "#FFF0DD",
                      		NONE      => "#000000",
                      		UNKNOWN   => "#000000",
                      	}
                      } );
has wind_colours    => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		HURRICANE => "#FFFFFF",
                      		STRONG    => "#CCCCCC",
                      		WINDY     => "#888888",
                      		BREEZE    => "#555555",
                      		CALM      => "#000000",
                      		UNKNOWN   => "#000000",
                      	}
                      } );
has snow_colours    => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		SNOW    => "#FFFFFF",
                      		NONE    => "#000000",
                      		UNKNOWN => "#000000",
                      	}
                      } );


# State is a hash of led groupname => colour; adjust it according to current WeatherForecast and our colours defined here.
sub render_into($self, $state)
{
	# Clear all weather-related indicators.
	foreach my $indicator (keys %{$self->weather_colours}) {
		$state->{$indicator} = "#000000";
	}
	
	# Handle SUN and SUN_UV indicators.
	$state->{SUN} = lerp($self->weather->max(), 15, 40, "#FFFFBB", "FF4444") unless $self->weather->max() == 0;
	$state->{SUN_UV} = $state->{SUN};
	$state->{SUN_UV} = "#FF55FF" if $self->weather->bom_uv;
	
	# Handle CLOUDY indicator.
	$state->{CLOUDY} = $self->cloud_colours->{$self->weather->cloudiness()} // "#FF55FF";	# Purple clouds means something is seriously amiss.
	
	# Handle RAINCLOUD, RAINLIGHT, RAINHEAVY indicators.
	my $rain_colours_str = $self->rain_colours->{$self->weather->raininess()} // "#FF55FF,#000000,#000000";
	( $state->{RAINCLOUD}, $state->{RAINLIGHT}, $state->{RAINHEAVY} ) = split(',', $rain_colours_str);
	
	# Handle LIGHTNING indicator.
	$state->{LIGHTNING} = $self->storm_colours->{$self->weather->storminess()} // "#FF55FF";

	# Handle WINDY indicator.
	$state->{WINDY} = $self->wind_colours->{$self->weather->windiness()} // "#FF55FF";

	# Handle SNOWFLAKE indicator.
	$state->{SNOWFLAKE} = $self->snow_colours->{$self->weather->snowiness()} // "#FF55FF";
	
	# Done! And we don't handle the WIFI indicator.
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
