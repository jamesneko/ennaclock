package WordTime;

# Module to get current time as which words on the clock to highlight.

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';


has word_colours => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		IT      => "#FFFFFF",
                      		J1      => "#FFFFFF",
                      		IS      => "#FFFFFF",
                      		A1      => "#FFFFFF",
                      		HALF    => "#FFFFFF",
                      		TEN_    => "#FFFFFF",
                      		QUARTER => "#FFFFFF",
                      		TWENTY  => "#FFFFFF",
                      		FIVE_   => "#FFFFFF",
                      		N1      => "#FFFFFF",
                      		MINUTES => "#FFFFFF",
                      		E1      => "#FFFFFF",
                      		TO      => "#FFFFFF",
                      		PAST    => "#FFFFFF",
                      		J2      => "#FFFFFF",
                      		ONE     => "#FFFFFF",
                      		TWO     => "#FFFFFF",
                      		THREE   => "#FFFFFF",
                      		FOUR    => "#FFFFFF",
                      		FIVE    => "#FFFFFF",
                      		SIX     => "#FFFFFF",
                      		SEVEN   => "#FFFFFF",
                      		EIGHT   => "#FFFFFF",
                      		NINE    => "#FFFFFF",
                      		TEN     => "#FFFFFF",
                      		ELEVEN  => "#FFFFFF",
                      		TWELVE  => "#FFFFFF",
                      		A2      => "#FFFFFF",
                      		OCLOCK  => "#FFFFFF",
                      	}
                      } );

# Easily map hour to word
my @hour_names = qw/TWELVE ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT NINE TEN ELEVEN TWELVE ONE/;


# State is a hash of led groupname => colour; adjust it according to current WordTime.
sub render_into($self, $state)
{
	my %words = $self->words();
	foreach my $word (keys %{$self->word_colours}) {
		# Set words we want ON, and words that aren't part of the current time OFF.
		$state->{$word} = $words{$word} // "#000000";
	}
}


# returns hash, word => hex colour
sub words($self)
{
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;
	my %cols = %{$self->word_colours()};
	
	# if the year is 1970, the pi has just booted and we haven't gotten NTP yet.
	if ($year + 1900 == 1970) {
		return ( IT => "#FF0000", IS => "#FF0000" );
	}
	
	# start off with 'IT IS', merging new entries into %hash via hash-slicing %cols.
	my %hash = %cols{qw/IT IS/};
	
	# looking forwards or backwards for purposes of hour display?
	if ($min >= 35) {
		$hour++;
	}
	$hour %= 12;
	my $hour_name = $hour_names[$hour];
	%hash = ( %hash, %cols{$hour_name} );
	
	# Now all the fiddly nitty-gritty of how much to/past or o'clockness we are at.
	if ($min < 5) {
		%hash = ( %hash, %cols{qw/OCLOCK/} );
	} elsif ($min < 10) {
		%hash = ( %hash, %cols{qw/FIVE_ MINUTES PAST/} );
	} elsif ($min < 15) {
		%hash = ( %hash, %cols{qw/TEN_ MINUTES PAST/} );
	} elsif ($min < 20) {
		%hash = ( %hash, %cols{qw/QUARTER PAST/} );
	} elsif ($min < 25) {
		%hash = ( %hash, %cols{qw/TWENTY MINUTES PAST/} );
	} elsif ($min < 30) {
		%hash = ( %hash, %cols{qw/TWENTY FIVE_ MINUTES PAST/} );
	} elsif ($min < 35) {
		%hash = ( %hash, %cols{qw/HALF PAST/} );
	} elsif ($min < 40) {
		%hash = ( %hash, %cols{qw/TWENTY FIVE_ MINUTES TO/} );
	} elsif ($min < 45) {
		%hash = ( %hash, %cols{qw/TWENTY MINUTES TO/} );
	} elsif ($min < 50) {
		%hash = ( %hash, %cols{qw/QUARTER TO/} );
	} elsif ($min < 55) {
		%hash = ( %hash, %cols{qw/TEN_ MINUTES TO/} );
	} elsif ($min < 60) {
		%hash = ( %hash, %cols{qw/FIVE_ MINUTES TO/} );
	} else {
		%hash = ( %hash, %cols{qw/OCLOCK/} );
	}
	
	return %hash;
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
