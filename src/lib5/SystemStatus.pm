package SystemStatus;

# Wrapper to run commands on the Raspberry Pi to check on system status, like wifi connectivity.
# Lotsa big hacks involving grepping output!

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';


has last_rx      => ( is => 'rw', isa => 'Int', default => 0 );
has wifi_colours => ( is => 'rw', isa => 'HashRef',
                      default => sub {
                      	{
                      		NOT_THERE   => "#FF5588",
                      		DOWNLOADING => "#3355BB",
                      		CONNECTED   => "#000000",	# Nothing to report, all is well.
                      		DUNNO       => "#FFAA33",
                      	}
                      } );


sub render_into($self, $state)
{
	$state->{WIFI} = $self->wifi_colours->{$self->wifi()} // "#FF55FF";
}


sub wifi($self)
{
	# Grab the ye olde string because `ip l` seemed to cause some stuttering in tests on the rpi?
	my $ifconfig = `ifconfig wlan0 2>&1`;

	## Monitor if we're downloading data over wifi.
	#my $last_rx = $self->last_rx();
	#my $this_rx = $last_rx;
	#if ($ifconfig =~ /RX bytes:(\d+)/) {
	#	$this_rx = $1;
	#}
	#$self->last_rx($this_rx);
	
	# netstat is probably a better indicator of whether there's actively some connections going on, actually
	my $netstat = `netstat --tcp 2>&1`;

	return "NOT_THERE" if $ifconfig =~ /\bDevice not found\b/;
	return "DOWNLOADING" if $netstat =~ /\bESTABLISHED\b/;
	return "CONNECTED" if $ifconfig =~ /\bUP\b/;
	return "DUNNO";
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;

