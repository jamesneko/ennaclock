package Cooldown;

use strict;
use warnings;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

require Exporter;
our @ISA = qw/Exporter/;
# use @EXPORT_OK to list the symbols to export on request only. @EXPORT is everything that gets exported by default.
our @EXPORT_OK = qw/cooldown resetting_cooldown cooldown_time_remaining/;
our %EXPORT_TAGS = ( all => [@EXPORT_OK] );


our $COOLDOWNS = {};


# Use as e.g. if Cooldown::cooldown("mocking_jest", 30);
# If less than 30 seconds has passed since the last mocking_jest cooldown call, returns a false value.
# Otherwise, returns $what (presumably a true value!) and activates the cooldown.
sub cooldown($what, $cooldown_secs)
{
	my $curr_time = time();
	my $last_time = $COOLDOWNS->{$what} // 0;
	my $interval = $curr_time - $last_time;
	if ($interval < $cooldown_secs) {
		# Fizzle.
		return undef;
	} else {
		# Worked! Activate cooldown.
		$COOLDOWNS->{$what} = $curr_time;
		return $what;
	}
}


# As per cooldown, but gets reset every time you call it forcing you to wait for e.g. 30 seconds of *no activity* before calling again.
sub resetting_cooldown($what, $cooldown_secs)
{
	if (my $rval = cooldown($what, $cooldown_secs)) {
		return $rval;
	} else {
		# Fizzled, reset the cooldown.
		$COOLDOWNS->{$what} = time();
		return $rval;
	}
}


# Check the current cooldown time remaining without affecting it.
sub cooldown_time_remaining($what, $cooldown_secs)
{
	my $curr_time = time();
	my $last_time = $COOLDOWNS->{$what} // 0;
	my $interval = $curr_time - $last_time;
	my $remaining = $cooldown_secs - $interval;
	$remaining = 0 if $remaining < 0;
	return $remaining;
}


1;
