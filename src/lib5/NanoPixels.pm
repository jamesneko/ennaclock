package NanoPixels;

# Wrapper to send commands to the Arduino Nano running the led strip code via Perl5's Device::SerialPort module.
# Certainly there must be easier ways to do this, I just don't know them.

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

use Try::Tiny;
use Device::SerialPort;
use Time::HiRes qw/sleep/;
use File::Glob ':bsd_glob';

use Helpers;

has port => ( is => 'rw' );


# Initialise the port itself.
sub init($self, $tty_override=undef)
{
	# Port likes to jump around if frequent connect/disconnects.
	my @ports = bsd_glob("/dev/ttyUSB*");
	die "No /dev/ttyUSB* devices present, is the Nano plugged in?" unless @ports || $tty_override;
	my $tty = $tty_override // $ports[0];

	$self->port(Device::SerialPort->new($tty) or die "Couldn't open $tty: $!\n");
	$self->port->user_msg(1);
	$self->port->error_msg(1);
	$self->port->baudrate(9600);	# 115200 seems too high for the little Nano.
	$self->port->parity("none");
	$self->port->databits(8);
	$self->port->stopbits(1);
	$self->port->write_settings;
	$self->port->lookclear;
	
	$self->port->read_char_time(0);     # don't wait for each character
	$self->port->read_const_time(1000); # 1 second per unfulfilled "read" call
}


# Handshake with the Arduino. Strange serial port oddities, here be dragons.
sub connect($self)
{
	my $rx = "";
	while ($rx ne "OHAI") {
		# For some odd voodoo serial bullshit reason, we have to send about 10 HELLO commands before we get a response.
		# Notably, we don't get 10 OHAIs back, either - those first few commands go down the memory hole.
		# I HAVE NO IDEA WHY. There's maybe some buffer we have to fill before some abstraction layer decides we
		# should get the go-ahead to send real data? Anyway... This works, eventually.
		$rx = $self->send("HELLO") // "";
	}
}


# Lower-level version of cmd() that doesn't expect anything *specific* in return.
# Should probably get *some* non-undef return value though, if things are well...
# Might have to reset the device if we get an undef.
sub send($self, $cmd)
{
	chomp $cmd;
	say "<- $cmd";
	$self->port->write($cmd . "\n");
	$self->port->lookclear; # I think this actually clears the buffer used by 'lookfor' gods these names.
	sleep(0.200);	# Float version supplied by Time::HiRes. Sleep here because... reasons.

	# Attempt to read a response. All responses from the Arduino should start with OK.
	my $attempts = 20;
	my $rx;
	while (--$attempts) {
		# Lookfor is 'designed to be polled in a loop' and by default 'looks for are match character \n'
		# Returns undef if some problem,
		#         "" if ... nothing yet? still matching?
		#         text if we... got a match?
		$rx = $self->port->lookfor();
		# I don't know where these fucking MS Windows CRs keep creeping in, does Arduino Serial do something to my \ns?
		$rx =~ s/\r//g;
		
		if ( ! defined $rx) {
			say "<- (undef)";
			return;
		}
		if ($rx ne "") {
			say "<- $rx";
			return $rx;
		}
		# Keep looking, until attempts run out and we 'time out'.
		print STDERR ".";
		sleep(0.200);
	}
	print STDERR "\n";
	return;
}


# Send a command and hang around until we get an OK back.
sub cmd($self, $cmd)
{
	my $rx = "";
	while ($rx !~ /^OK/) {
		# rx will not start with OK if there's an error; undefined indicates Bad Stuff, need to restart.
		# or "Available commands: " indicates there was some transmission corruption, we can try resend.
		$rx = $self->send($cmd);
		if ( ! defined $rx) {
			# rx will only be undef IF
			# a) send() lookfor returns undef, indicating connection-level error of some kind
			# b) send() $attempts reaches 0, indicating we timed-out.
			#    Timeouts are normal while waiting for HELLO/OHAI to warm up, but timeouts
			#    during normal command sending seem to indicate the USB connection got reset for some reason.
			#    In that case, we need to report the problem higher-up so that handshakes can be re-performed
			#    and device state can be resent.
			die "Failed to send '$cmd': Possible connection loss.\n";
		}
	}
	return $rx;
}


sub group($self, $g)
{
	$self->cmd("GROUP $g");
}


sub leds($self, $start, $len)
{
	$self->cmd("LEDS $start $len");
}


sub set($self, $r, $g, $b)
{
	$self->cmd("SET $r $g $b");
}


sub fade($self, $r, $g, $b, $ticks)
{
	$self->cmd("FADE $r $g $b $ticks");
}


sub flash($self, $r, $g, $b, $ticks)
{
	$self->cmd("FLASH $r $g $b $ticks");
}


sub bounce($self, $r, $g, $b, $ticks)
{
	$self->cmd("BOUNCE $r $g $b $ticks");
}


sub starfield($self, $r, $g, $b, $ticks)
{
	$self->cmd("STARFIELD $r $g $b $ticks");
}


sub lightning($self, $r, $g, $b, $ticks, $chance)
{
	$self->cmd("LIGHTNING $r $g $b $ticks $chance");
}


sub clear($self)
{
	$self->cmd("GROUP ALL");
	$self->cmd("LEDS 0 0");
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
