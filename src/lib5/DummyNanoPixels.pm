package DummyNanoPixels;

# Same interface to NanoPixels but doesn't require an actual nano to be plugged in.

use Moose;
use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

use Try::Tiny;
use Time::HiRes qw/sleep/;
use Term::ANSIColor;

use Helpers;

has port => ( is => 'rw' );


sub mutter
{
	note colored("DummyNanoPixels: " . join(' ', @_), 'yellow');
}


# Initialise the port itself.
sub init($self, $tty_override="<default tty>")
{
	mutter "init($tty_override)";
}


# Handshake with the Arduino. Strange serial port oddities, here be dragons.
sub connect($self)
{
	mutter "connect()";
}


# Lower-level version of cmd() that doesn't expect anything *specific* in return.
# Should probably get *some* non-undef return value though, if things are well...
# Might have to reset the device if we get an undef.
sub send($self, $cmd)
{
	mutter "<- $cmd";
	sleep(0.200);	# Float version supplied by Time::HiRes. Sleep here because... reasons.
	return "OK";
}


# Send a command and hang around until we get an OK back.
sub cmd($self, $cmd)
{
	return $self->send($cmd);
}


sub group($self, $g)
{
	$self->cmd("GROUP $g");
}


sub leds($self, $start, $len)
{
	$self->cmd("LEDS $start $len");
}


sub set($self, $r, $g, $b)
{
	$self->cmd("SET $r $g $b");
}


sub fade($self, $r, $g, $b, $ticks)
{
	$self->cmd("FADE $r $g $b $ticks");
}


sub flash($self, $r, $g, $b, $ticks)
{
	$self->cmd("FLASH $r $g $b $ticks");
}


sub bounce($self, $r, $g, $b, $ticks)
{
	$self->cmd("BOUNCE $r $g $b $ticks");
}


sub starfield($self, $r, $g, $b, $ticks)
{
	$self->cmd("STARFIELD $r $g $b $ticks");
}


sub lightning($self, $r, $g, $b, $ticks, $chance)
{
	$self->cmd("LIGHTNING $r $g $b $ticks $chance");
}


sub clear($self)
{
	$self->cmd("GROUP ALL");
	$self->cmd("LEDS 0 0");
}


no Moose;
__PACKAGE__->meta->make_immutable;
1;
