#!/usr/bin/env perl6

use v6;
use lib $*PROGRAM-NAME.IO.dirname.IO.abspath ~ '/../lib6';
use NanoPixels;

sub MAIN(Str $tty = "/dev/ttyUSB0")
{
	my $pix = NanoPixels.new;
	$pix.init($tty);
	$pix.connect();
	$pix.clear();

	$pix.group(0);
	$pix.leds(0, 30);
	
	while (True) {
		my $r = (0..255).pick;
		my $g = (0..255).pick;
		my $b = (0..255).pick;
		$pix.set($r, $g, $b);
		$pix.fade(0, 0, 0, 20);
		sleep(1.000);
	}
}
