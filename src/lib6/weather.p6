#!/usr/bin/env perl6

use v6;
use lib $*PROGRAM-NAME.IO.dirname.IO.abspath ~ '/../lib6';
use WeatherForecast;

my $CFG_DIR = $*PROGRAM-NAME.IO.dirname.IO.abspath ~ '/../cfg';


sub MAIN()
{
	my $apikey = chomp slurp($CFG_DIR ~ '/openweathermap.apikey') or die "Couldn't read cfg/openweathermap.apikey!";
   note "OpenWeatherMap API Key: $apikey";
	
	my $weather = WeatherForecast.new(:$apikey);
	$weather.refresh();

	note "Cloudiness: " ~ $weather.cloudiness();
	note "Raininess:  " ~ $weather.raininess();
	note "Windiness:  " ~ $weather.windiness();
	note "Temperature min: " ~ $weather.min() ~ " max: " ~ $weather.max();
}

