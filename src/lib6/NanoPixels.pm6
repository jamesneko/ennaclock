unit class NanoPixels;

# Wrapper to send commands to the Arduino Nano running the led strip code via Perl5's Device::SerialPort module.
# Certainly there must be easier ways to do this, I just don't know them.

use Inline::Perl5;
use Device::SerialPort:from<Perl5>;

# All RGB values take 0..255 only.
subset RGB of Int where 0..255;


has $.port;

# Initialise the port itself.
method init(Str $tty)
{
	$!port = Device::SerialPort.new($tty) or die "Couldn't open $tty: $!\n";
	$!port.user_msg(1);
	$!port.error_msg(1);
	$!port.baudrate(115200);
	$!port.parity("none");
	$!port.databits(8);
	$!port.stopbits(1);
	$!port.write_settings;
	$!port.lookclear;
}


# Handshake with the Arduino. Strange serial port oddities, here be dragons.
method connect()
{
	my $rx = "";
	while $rx !~~ "OHAI" {
		# For some odd voodoo serial bullshit reason, we have to send about 10 HELLO commands before we get a response.
		# Notably, we don't get 10 OHAIs back, either - those first few commands go down the memory hole.
		# I HAVE NO IDEA WHY. There's maybe some buffer we have to fill before some abstraction layer decides we
		# should get the go-ahead to send real data? Anyway... This works, eventually.
		$rx = self.send("HELLO");
	}
}


# Lower-level version of cmd() that doesn't expect anything in return.
method send(Str $cmd is copy)
{
	$cmd = chomp $cmd;
	say "<- $cmd";
	$!port.write($cmd ~ "\n");
	$!port.lookclear; # needed to prevent blocking allegedly
	sleep(0.200);	# Because... reasons.

	# Attempt to read a response. All responses from the Arduino should start with OK.
	my Str $rx;
	my Int $bytes-read;
	($bytes-read, $rx) = $!port.read(255);
	if $bytes-read > 0 {
		$rx = chomp $rx;
		say "-> $rx";
		return $rx;
	} else {
		return;
	}
}


# Send a command and hang around until we get an OK back.
method cmd(Str $cmd is copy)
{
	my Str $rx = self.send($cmd) // "";
	my Int $bytes-read;
	while ($rx !~~ /^OK/) {
		($bytes-read, $rx) = $!port.read(255);
		$rx //= "";
	}
	return $rx;
}


method group(UInt $g)
{
	self.cmd("GROUP $g");
}


method leds(UInt $start, UInt $len)
{
	self.cmd("LEDS $start $len");
}


method set(RGB $r, RGB $g, RGB $b)
{
	self.cmd("SET $r $g $b");
}


method fade(RGB $r, RGB $g, RGB $b, Int $ticks where 0..255)
{
	self.cmd("FADE $r $g $b $ticks");
}


method bounce(RGB $r, RGB $g, RGB $b, Int $ticks where 0..255)
{
	self.cmd("BOUNCE $r $g $b $ticks");
}


method starfield(RGB $r, RGB $g, RGB $b, Int $ticks where 0..255)
{
	self.cmd("STARFIELD $r $g $b $ticks");
}


method clear()
{
	self.cmd("GROUP ALL");
	self.cmd("LEDS 0 0");
}


