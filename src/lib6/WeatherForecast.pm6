unit class WeatherForecast;

use LWP::Simple;
use JSON::Fast;

# Fuzzy value for weather intensities
enum Cloudiness <CLEAR FEW MEDIUM VERY>;
enum Raininess  <NONE LIGHT MODERATE HEAVY TORRENTIAL FREEZING>;
enum Windiness  <CALM BREEZE WINDY HURRICANE>;

has Str $.apikey is required;
has Str $.location = "Sydney,au";
has $.result;
has $.debug = True;

method refresh()
{
	my $lwp = LWP::Simple.new;
	my $url = "http://api.openweathermap.org/data/2.5/forecast?"
					~ "q=" ~ $!location
	####			~ "lat=-33&lon=-151"
					~ "&appid=" ~ $.apikey
					~ "&units=metric";
	note "GET: $url";
	my $text = $lwp.get($url);
	die "No response from server!" unless $text;
	$!result = from-json $text;
	#note "Result: " ~ $!result.gist;
	note "";

	if $.debug {
		# List weather for each datetime entry.
		# Separate by day.
		my $day;
		for $!result<list>.flat -> $entry {
			my $time = DateTime.new($entry<dt>).local;
			$day //= $time.day;
			if $day != $time.day {
				note "";
				$day = $time.day;
			}
			my $min = $entry<main><temp_min>.fmt("%.02f");
			my $max = $entry<main><temp_max>.fmt("%.02f");
			my $wind = $entry<wind><speed>.fmt("%.02f");
			# See http://openweathermap.org/weather-conditions for all weather codes.
			my @weathers = $entry<weather>.list.map: { "$_<id> $_<main> - $_<description>" };
			note "$time: $min - $max, $wind m/s, " ~ @weathers;
		}
	}
}


# Return list of hash entries just for the interesting bits of (the rest of) today's forecast.
method today()
{
	my @results;
	my $day;
	for $!result<list>.flat -> $entry {
		my $time = DateTime.new($entry<dt>).local;
		$day //= $time.day;
		if $day != $time.day {
			last;
		}
		my $min = $entry<main><temp_min>.fmt("%.02f");
		my $max = $entry<main><temp_max>.fmt("%.02f");
		my $wind = $entry<wind><speed>.fmt("%.02f");
		# See http://openweathermap.org/weather-conditions for all weather codes.
		my @weathers = $entry<weather>.list.map: { "$_<id> $_<main> - $_<description>" };
		push @results, { :$time, :$day, :$min, :$max, :$wind, weather => @weathers[0] };
	}
	return @results;
}


method cloudiness() returns Cloudiness
{
	my @entries = self.today();
	my $cloudcount = 0;
	for @entries -> $e {
		given $e<weather> {
			when /'overcast clouds'/ { $cloudcount += 3 }
			when /'broken clouds'/   { $cloudcount += 2 }
			when /'clouds'/          { $cloudcount += 1 }
			when /'rain'/            { $cloudcount += 1 }
		}
	}
	given $cloudcount {
		when    * > 10 { return VERY;   }
		when    * > 5  { return MEDIUM; }
		when    * > 2  { return FEW;    }
		default        { return CLEAR;   }
	}
}


method raininess() returns Raininess
{
	my @entries = self.today();
	my $raincount = 0;
	my $freezingcount = 0;
	for @entries -> $e {
		given $e<weather> {
			#### TODO: I suspect Thunderstorm codes should imply raininess too.
			when m:s/(extreme) rain/               { $raincount += 4 }
			when m:s/(heavy intensity|heavy) rain/ { $raincount += 3 }	# 'with heavy rain' also shows up in Thunderstorm codes.
			when m:s/(moderate) rain/              { $raincount += 2 }
			when m:s/(light|shower) rain/          { $raincount += 1 }
			when m:s/drizzle/                      { $raincount += 1 }	# found in the Thunderstorm codes.
		}
	}
	return FREEZING if $freezingcount > 2;		#### BUG: Never reached. Needs case above?
	given $raincount {
		when    * > 15 { return TORRENTIAL; }
		when    * > 10 { return HEAVY;      }
		when    * > 5  { return MODERATE;   }
		when    * > 1  { return LIGHT;      }
		default        { return NONE;       }
	}
}


method windiness() returns Windiness
{
	my @entries = self.today();
	# Extract and Reduce wind speeds using the max operator.
	#### Is this a brainfart? Can max not just work on a list anyway?
	my $maxwind = [max] @entries.map: { $_<wind> };
	given $maxwind {
		when    * > 8 { return HURRICANE; }
		when    * > 4 { return WINDY;     }
		when    * > 1 { return BREEZE;    }
		default       { return CALM;      }
	}
}


method min()
{
	my @entries = self.today();
	# Extract and Reduce min temperatures using the min operator.
	#### Is this a brainfart? Can max not just work on a list anyway?
	my $min = [min] @entries.map: { $_<min> };
	return $min;
}


method max()
{
	my @entries = self.today();
	# Extract and Reduce max temperatures using the max operator.
	#### Is this a brainfart? Can max not just work on a list anyway?
	my $max = [max] @entries.map: { $_<max> };
	return $max;
}

