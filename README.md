# Enna Clock

A mixture of Arduino C++ and Perl code intended for a raspberry pi to control a word-clock display powered by an Adafruit NeoPixel strip.

Arduino code accepts serial commands to modify LED state. Pi or computer does all the more clever stuff.

For Enna.
